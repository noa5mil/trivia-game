import axios from "axios";

const axiosInstance = axios.create({ baseURL: '/api/games' });

export const startGame = async () => {
    const response = await axiosInstance.post('/init-game');
    return response.data;
}

export const startGameFromCookie = async (gameId) => {
    const response = await axiosInstance.get(`/init-game/${gameId}`);
    return response.data;
}

export const checkGuess = async (guess, gameId) => {
    const response = await axiosInstance.patch(`/attempt/${gameId}`, { guess });
    return response.data;
}