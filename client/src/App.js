import TriviaGame from './screens/TriviaGame/TriviaGame';

function App() {
  return (
    <TriviaGame />
  );
}

export default App;
