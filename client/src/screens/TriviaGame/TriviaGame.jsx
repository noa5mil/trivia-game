import React, { useState, useEffect } from 'react';
import Cookies from 'js-cookie';
import { startGame, checkGuess, startGameFromCookie } from '../../services/scores';
import './TriviaGame.css';

const COOKIE_KEY = 'game';

function TriviaGame() {
    const [rounds, setRounds] = useState(undefined);
    const [attempt, setAttempt] = useState(1);
    const [gameRound, setGameRound] = useState(1);
    const [gameId, setGameId] = useState(undefined);
    const [albumNames, setAlbumNames] = useState([]);
    const [albumPic, setAlbumPic] = useState('');
    const [playerPoints, setPlayerPoints] = useState(0);
    const [guess, setGuess] = useState('');

    useEffect(() => {
        if (rounds === gameRound) {
            Cookies.remove(COOKIE_KEY);
        }
    }, [rounds, gameRound]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                let data;
                if (Cookies.get(COOKIE_KEY)) {
                    data = await startGameFromCookie(Cookies.get(COOKIE_KEY));
                } else {
                    data = await startGame();
                }
                setGameId(data.gameId);
                setAlbumNames(data.albumName);
                setRounds(data.rounds);
                data.pic && setAlbumPic(data.pic);
                data.points && setPlayerPoints(data.points);
                data.round && setGameRound(data.round);

                if (!Cookies.get(COOKIE_KEY)) {
                    Cookies.set(COOKIE_KEY, data.gameId);
                }
            }
            catch (error) {
                // TODO Error handling.
                console.log(error);
            }
        }
        if (!gameId) {
            fetchData();
        }
    }, []);

    const handleOnChange = (e) => {
        setGuess(e.target.value);
    };

    const handleRestart = () => {
        window.location.reload(false);
    };

    const handleOnClick = async () => {
        try {
            const data = await checkGuess(guess, gameId);
            if (data.round !== gameRound) {
                // New round started.
                setAttempt(1);
                setGameRound(data.round);
                setAlbumPic('');
            }
            setAlbumNames(data.albumName);
            setPlayerPoints(data.points);
            setAttempt(attempt + 1);
            setGuess('');
            data.pic && setAlbumPic(data.pic);
        } catch (error) {
            // TODO Error handling.
            console.log(error);
        }
    };

    return (
        <>
            <h3>Guess The Artist</h3>
            {rounds === gameRound && (
                <button onClick={handleRestart}>Restart</button>
            )}
            {rounds > gameRound && (
                <div className="gameBoard">
                    <div className="albums">
                        <h4>{`Round ${gameRound}`}</h4>
                        <ul>
                            {albumNames?.map((albumName, index) => <li key={albumName + index}>{albumName}</li>)}
                        </ul>
                        {albumPic && <img src={albumPic} alt='Album Pic' />}
                    </div>
                    <div className='playerSpace'>
                        <div>
                            <label htmlFor='guess'>Who is the artist?</label>
                        </div>
                        <input id='guess' onChange={handleOnChange} value={guess}>
                        </input>
                        <button onClick={handleOnClick}>Submit</button>
                    </div>
                </div>
            )}
            <div>Points: {playerPoints}</div>
        </>
    );
};

export default TriviaGame;
