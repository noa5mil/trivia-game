const artistsList = [
    {
        iTuneId: '909253',
        name: 'Jack Johnson',
    },
    {
        iTuneId: '271256',
        name: 'Madonna',
    },
    {
        iTuneId: '217005',
        name: 'Britney Spears',
    },
    {
        iTuneId: '27496674',
        name: 'James Blunt',
    },
    {
        iTuneId: '1798556',
        name: 'Maroon 5',
    },
];

const pointsByAttempt = {
    1: 5,
    2: 3,
    3: 1,
}

const ItunesAttributes = {
    wrapperType: 'collection',
    entity: 'album'
}

const gameConfig = {
    attempts: 3,
    rounds: 5,
}


module.exports = { artistsList, pointsByAttempt, ItunesAttributes, gameConfig };