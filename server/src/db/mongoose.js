const mongoose = require('mongoose');
const Artist = require('../models/artist');
const { artistsList } = require('../utils/game');

mongoose.connect("mongodb://127.0.0.1:27017/trivia-game", { useNewUrlParser: true })
.then(() => {
    mongoose.connection.db.listCollections({ name: 'artists' })
        .next(function (err, collectionInfo) {
            if (!collectionInfo) {
                insertArtists();
            }
        });
})
.catch((err) => console.log(err));

async function insertArtists() {
    const documents = artistsList.map(artistData => {
        return {
            insertOne: {
                document: {
                    iTuneId: artistData.iTuneId,
                    name: artistData.name,
                }
            }
        }
    });
    Artist.bulkWrite(documents)
    .then(() => console.log('DB initialized'))
    .catch((err) => console.log(err));
};
