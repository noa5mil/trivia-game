const Game = require('../models/game');
const Artist = require('../models/artist');
const axios = require('axios');
const { ItunesAttributes, gameConfig } = require('../utils/game');

const axiosInstance = axios.create({ baseURL: 'https://itunes.apple.com/lookup' });

const getFirstArtistAlbum = async (artistDocumentId) => {
    const artistDocumentAttributes = await Artist.findById(artistDocumentId).lean();
    if (artistDocumentAttributes.albumNames.length !== 0) {
        return artistDocumentAttributes.albumNames[0];
    }

    const response = await axiosInstance.get(
        `?id=${artistDocumentAttributes.iTuneId}
        &entity=${ItunesAttributes.entity}
        &limit=${gameConfig.attempts}`);
    const albums = response.data.results.filter(result => result.wrapperType === ItunesAttributes.wrapperType);

    await Artist.findByIdAndUpdate(artistDocumentAttributes._id,
        {
            albumNames: albums.map(album => album.collectionName),
            albumPicks: albums.map(album => album.artworkUrl60),
        })
    return albums[0].collectionName;
};

const createGameDocument = async (artistsDocuments) => {
    const game = new Game({
        artists: artistsDocuments.map(artistsDocument => artistsDocument._id),
    });
    await game.save();
    return game._id;
};

function getRandomElements(arr, size) {
    const shuffled = [...arr].sort(() => 0.5 - Math.random());
    return shuffled.slice(0, size);
};


module.exports = {
    getFirstArtistAlbum,
    createGameDocument,
    getRandomElements,
};