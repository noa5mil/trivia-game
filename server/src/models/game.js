const mongoose = require('mongoose');

const gameScheme = new mongoose.Schema({
    username: {
        type: String,
        trim: true,
    },
    points: {
        type: Number,
        default: 0
    },
    currentRound: {
        type: Number,
        default: 1,
    },
    currentAttempt: {
        type: Number,
        default: 1,
    },
    artists: {
        type: Array,
    },
}, {
    timestamps: true,
});

const Game = mongoose.model('Game', gameScheme);
module.exports = Game;