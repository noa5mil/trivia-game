const mongoose = require('mongoose');

const artistScheme = new mongoose.Schema({
    iTuneId: {
        type: String,
        trim: true,
        require: true,
    },
    name: {
        type: String,
        trim: true,
        require: true,
    },
    albumNames: {
        type: Array,
    },
    albumPicks: {
        type: Array,
    },
}, {
    timestamps: true,
});

const Artist = mongoose.model('Artist', artistScheme);
module.exports = Artist;