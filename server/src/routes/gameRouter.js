const express = require('express');
const Artist = require('../models/artist');
const Game = require('../models/game');
const { artistsList, pointsByAttempt, gameConfig } = require('../utils/game');
const {
    getFirstArtistAlbum,
    createGameDocument,
    getRandomElements,
} = require('../services/gameService');

const router = new express.Router();

router.patch('/games/attempt/:game', async (req, res) => {
    try {
        const { guess } = req.body;
        const gameId = req.params.game;
        const game = await Game.findById(gameId).lean();

        const artistOfRound = await Artist.findById(game.artists.at(game.currentRound - 1)).lean();

        if (game.currentRound === gameConfig.rounds) {
            throw new Error('Game Over');
        }

        let attempt = game.currentAttempt;
        let round = game.currentRound;
        let albumName = artistOfRound.albumNames?.slice(0, game.currentAttempt + 1)
        const gameUpdates = {};
        const didWin = guess.toLowerCase() === artistOfRound.name.toLowerCase();

        if (didWin || game.currentAttempt === gameConfig.attempts) {
            if (didWin) {
                gameUpdates['points'] = game.points + pointsByAttempt[game.currentAttempt]
            }
            gameUpdates['currentAttempt'] = 1
            gameUpdates['currentRound'] = game.currentRound + 1
            attempt = 1
            round = game.currentRound + 1

            if (round < gameConfig.rounds) {
                albumName = [await getFirstArtistAlbum(game.artists.at(round - 1))];
            }
            else {
                albumName = '';
            }
        }
        else {
            gameUpdates['currentAttempt'] = game.currentAttempt + 1;
        }

        await Game.findByIdAndUpdate(gameId, gameUpdates);

        res.status(200).send({
            albumName: albumName,
            pic: attempt == gameConfig.attempts - 1 ? artistOfRound.albumPicks[attempt] : '',
            points: didWin ? gameUpdates['points'] : game.points,
            round: round,
        });
    } catch (err) {
        console.log(err);
        res.status(400).send(err);
    }
})

router.get('/games/init-game/:gameId', async (req, res) => {
    // TODO Get the game id from the cookie.
    try {
        const gameId = req.params.gameId;
        const game = await Game.findById(gameId).lean();
        if (game.currentRound === gameConfig.rounds) {
            throw new Error('Game Over');
        }

        const artistOfRound = await Artist.findById(game.artists.at(game.currentRound - 1)).lean();
        res.status(200).send({
            albumName: artistOfRound.albumNames.slice(0, game.currentAttempt),
            pic: game.currentAttempt === gameConfig.attempts ? artistOfRound.albumPicks[game.currentAttempt - 1] : '',
            gameId: gameId,
            rounds: gameConfig.rounds,
            round: game.currentRound,
            points: game.points,
        });
    } catch (err) {
        console.log(err);
        res.status(400).send(err);
    }
});

router.post('/games/init-game', async (req, res) => {
    try {
        const gameArtists = getRandomElements(artistsList, gameConfig.rounds - 1);
        let artistsDocuments = await Artist.find({
            iTuneId: { $in: gameArtists.map(artist => artist.iTuneId) }
        }).lean();

        function sortArtistsByIndex(artistA, artistB) {
            return gameArtists.findIndex(e => e.iTuneId === artistA.iTuneId)
                - gameArtists.findIndex(e => e.iTuneId === artistB.iTuneId);
        };

        artistsDocuments.sort(sortArtistsByIndex);

        const [firstAlbumName, gameId] = await Promise.all([
            getFirstArtistAlbum(artistsDocuments[0]._id),
            createGameDocument(artistsDocuments),
        ]);

        res.status(200).send({
            albumName: [artistsDocuments[0]?.albumNames?.[0] || firstAlbumName],
            gameId: gameId,
            rounds: gameConfig.rounds,
        });
    } catch (err) {
        console.log(err);
        res.status(400).send(err);
    };
});

module.exports = router;