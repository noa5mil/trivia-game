const express = require('express');
const gameRouter = require('./src/routes/gameRouter');
require('./src/db/mongoose');

const app = express();

app.use(express.json());
app.use('/api', gameRouter);

module.exports = app;
